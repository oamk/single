<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <?php
  $eurot = 0;
  $punnat = 0;
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $eurot = filter_input(INPUT_POST,'eurot',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
    $punnat = $eurot * 0.9;
  }
  ?>
  <h3>Valuuttalaskuri</h3>
  <form action="index.php" method="post">
    <div>
      <label>Eurot</label>
      <input name="eurot" value="<?php printf('%.2f',$eurot);?>"/>
    </div>

    <button>Laske</button>
    <div>
      <label>Punnat</label>
      <output>
      <?php
      printf("%.2f",$punnat);
      ?>
      </output>
    </div>
  </form>
</body>
</html>